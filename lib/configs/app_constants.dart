abstract class AppConstants {
  static const String applicationName = 'islieb Comic Reader';
  static const String rssUrl = 'https://islieb.de/zeige/comicapp/feed/';
  static const String boxName = 'rss_cache';
  static const String website = 'https://islieb.de';
}
